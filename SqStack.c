#include "SqStack.h"

/*
 * 初始化
 *
 * 构造一个空栈。初始化成功则返回OK，否则返回ERROR。
 */
Status InitStack(SqStack* S)
{
	if (S == NULL)
		return ERROR;

	S->base = (SElemType*)malloc(STACK_INIT_SIZE * sizeof(SElemType));
	if (S->base == NULL)
		exit(1);

	S->top = S->base;
	S->stacksiaze = STACK_INIT_SIZE;
	return OK;
}

/*
 * 判空
 *
 * 判断顺序栈中是否包含有效数据。
 *
 * 返回值：
 * TRUE : 顺序栈为空
 * FALSE: 顺序栈不为空
 */
Status StackEmpty(SqStack S)
{
	if (S.base == S.top)
		return TRUE;
	return FALSE;
}

/*
 * 入栈
 *
 * 将元素e压入到栈顶。
 */
Status Push(SqStack* S, SElemType e)
{
	if (S->base == NULL || S == NULL)
		return ERROR;

	//若栈满了，则增加空间
	if (S->top - S->base >= S->stacksiaze)
	{
		S->base = (SElemType*)realloc(S->base,(S->stacksiaze + STACKINCREMENT) * sizeof(SElemType));
		if (S->base == NULL)
			exit(1);//分配失败

		S->top = S->base + S->stacksiaze;
		S->stacksiaze += STACKINCREMENT;
	}

	//将e压入栈
	*(S->top++) = e;

	return OK;
}

/*
 * 出栈
 *
 * 将栈顶元素弹出，并用e接收。
 */
Status Pop(SqStack* S, SElemType* e)
{
	if (S->base == NULL || S == NULL || S->base == S->top)
		return ERROR;

	//栈顶指针先递减
	*e = *(--(S->top));

	return OK;
}