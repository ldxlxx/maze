#ifndef MAZE_H
#define MAZE_H

#include "SqStack.h"

#define SleepTime 3

#define M 15	//迷宫行数
#define N 15	//迷宫列数

#define X 4		//X指示迷宫障碍出现的概率。X=4，意味着遍历迷宫时遇到障碍的概率是1/4=25%

typedef enum {
	Wall,						//外墙
	Obstacle,					//迷宫内墙
	Way,						//通路
	Impasse,					//死胡同
	East, South, West, North	//方向（东南西北）
} MazeNode;

typedef int MazeType[M][N];//迷宫类型

Status MazePath(MazeType maze, PosType start, PosType end);//地图寻路
void InitMaze(MazeType maze, PosType* start, PosType* end);//初始化地图
Status Pass(MazeType maze, PosType seat);//该位置能否通过
PosType NextPos(PosType seat, MazeNode di);//下一个方向
void FootPrint(MazeType maze, PosType seat);
void MarkPrint(MazeType maze, PosType seat, int mark);
void Wait(long time);//延时
void PrintMaze(MazeType maze);//打印地图
Status Equals(PosType a, PosType b);//比交
SElemType Construct(PosType seat, int di);
#endif
