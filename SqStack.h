#ifndef SQTACK_H
#define SQTACK_H

#include <stdio.h>
#include <stdlib.h>     // 提供malloc、realloc、free、exit原型

#define ERROR 0;
#define OK 1;

#define TRUE 1;
#define FALSE 0;



#define STACK_INIT_SIZE 100     // 顺序栈存储空间的初始分配量
#define STACKINCREMENT  10      // 顺序栈存储空间的分配增量


typedef int Status;


//通道坐标
typedef struct {
	int x;
	int y;
}PosType;

//通道信息
typedef struct {
	PosType seat;
	int di;//下一个访问的方向
}SElemType;

typedef struct {
	SElemType* top;//栈顶指针
	SElemType* base;//栈底指针
	int stacksiaze;//当前已分配的存储空间大小，元素为单位
}SqStack;

Status InitStack(SqStack* S);//初始化栈
Status StackEmpty(SqStack S);//判断是否有有效数据
Status Push(SqStack* S, SElemType e);//入栈
Status Pop(SqStack* S, SElemType* e);//出栈

#endif
